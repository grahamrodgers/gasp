from gasp import *

begin_graphics()
finished = False

player_shape = 5
def place_player(player_x, player_y, player_shape):
    print("Here I am!")
    Circle((10*player_x+5, 10*player_y+5), player_shape, filled=True)

def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == '6' and player_x < 63:
        player_x += 1
    elif key == '3':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1


    move_to(player_shape, (10*player_x+5, 10*player_y+5))

player_x = random_between(0, 63)
player_y = random_between(0, 47)
place_player(player_x, player_y, player_shape)

while not finished:
    move_player()

end_graphics()
